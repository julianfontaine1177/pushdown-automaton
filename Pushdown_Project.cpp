#include <iostream>
#include <stdio.h> 
#include <stdlib.h>
#include <string>
using namespace std;

//The language is L = {a^nb^n : n > 0} ∪ {b, bb}

class stack
{
public:
	enum {sizeStack = 11};
	void init() {top = -1;}
	void push(int n){
		stackArr[++top] = n;
		return;
	}
	int pop(){
		stackArr[top--];
	}
	bool isEmpty() {return top < 0 ? 1 : 0;}

	int peek(){
		if(isEmpty() ) {
      cerr << "\tEmpty Stack. Don't Pop\n\n";
      return 1;
    	}
    	return stackArr[top];
	}
	void dump_stack() {
    // cout << "The Stack contents, from top to bottom, from a stack dump are: " << endl;
    for (int i = top; i >= 0; i--)
      cout << "\t\t" << stackArr[i] << endl;
  }
	// stack();
	// ~stack();
private:
	int top;
	int stackArr[sizeStack];
};

int main()
{
	string buffer = "";// the buffer is the history for the program
	string end;
	char input;
	int  state = 0;
	// std::vector<int> stackArr;
	// enum {sizeStack = 11};
	// int stackArr[sizeStack];
	stack arr_stack;
	arr_stack.init();
	
	// arr_stack.push();
	// arr_stack.pop();

	cout << "NFA Alphabet: E={a, b}\nSo you must enter in either a or b";
	cout << "Accepted Langauges: L={aabb, bbbbb}\n";
	cout << "\nYou are in state qs\n";
	arr_stack.push(0);// program begins with 0 in the stack
	arr_stack.dump_stack();

	while(1) {		// endless loop
		while(state == 0) // loop for start state
		{
			cin >> input;

			if(input != 'a' && input != 'b' && input != ' ') {	// stops the user from entering anything other then a, b, and empty set
					exit(0);
			}

			if (input == 'a')// loop for q1
			{

				cout << "You are in state q1\n";
				arr_stack.push(1);
				buffer+=input;

				arr_stack.dump_stack();
			}

			if(arr_stack.peek() == 0) {
				cout << "You are in state qt\n";
				cin >> input;
				if(input != 'a' && input != 'b') {
					exit(0);
				}

				if (input == 'a' || input == 'b') {
					cout << "You are in state qt\n";
					state+=2;
					// exit(0);
				}
			}	

			if (input == 'b') {
					if(arr_stack.peek() >= 1) {
						arr_stack.pop();

						arr_stack.dump_stack();

						state++;
						buffer+=input;
					}	
			}

			if(arr_stack.peek() == 0) {
				cout << "You are in state qt\n";
				cin >> input;
				if(input != 'a' && input != 'b') {
					exit(0);
				}

				if (input == 'a' || input == 'b') {
					cout << "You are in state qt\n";
					exit(0);
				}
			}

			while(state == 1)//loop for q2
			{
				cout << "You are in state q2\n";
				cin >> input;
				if(input != 'a' && input != 'b') {
				exit(0);
				}
				if (input == 'b'){
					if(arr_stack.peek() >= 0) {
						arr_stack.pop();

						arr_stack.dump_stack();

						buffer+=input;
						// cout << "You are in state q2\n";
					}	
					if(arr_stack.peek() == 0) {
						state++;
					}
				}

				if(input == 'a') {
					cout << "You are in state qt\n";
					cin >> input;
					if(input != 'a' && input != 'b') {
					exit(0);
				}

				if(input == 'a' || input == 'b') {
					cout << "You are in state qt\n";
					exit(0);
					}
				}
			}

			while(state == 2){
				arr_stack.pop();
				cout << "\nStack:\n";
				arr_stack.dump_stack();
				cout << "Pushdown Langauge: ";
				cout << buffer <<endl;
				exit(0);
			}
		}
	}
	return 0;
}